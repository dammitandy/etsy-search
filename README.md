# Setup

- Install the [Lombok IntelliJ plugin](http://plugins.jetbrains.com/plugin/6317) to add IDE support for [Lombok](http://projectlombok.org/features/index.html). Ugly web site, awesome annotation library.