package org.andydyer.etsysearch;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;
import com.etsy.android.grid.util.DynamicHeightImageView;
import com.squareup.picasso.Picasso;

import org.andydyer.etsysearch.api.ApiService;
import org.andydyer.etsysearch.api.Listing;
import org.andydyer.etsysearch.api.ListingResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 *
 */
public class ListingsFragment extends Fragment implements Callback<ListingResponse> {

    protected static final int PAGE_SIZE = 25;

    private static String ARG_QUERY = "query";

    @Inject ApiService apiService;

    @InjectView(R.id.grid_view) StaggeredGridView gridView;

    private ListingsAdapter adapter;

    public static ListingsFragment newInstance(String query) {
        ListingsFragment fragment = new ListingsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_QUERY, query);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        EtsySearchApplication.getInstance().inject(this);
        adapter = new ListingsAdapter(getActivity(), new ArrayList<Listing>());
        getActivity().setProgressBarIndeterminateVisibility(true);
        requestListings();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Listing listing = adapter.getItem(position);
                launchWebViewActivity(listing);
            }
        });
    }

    protected void requestListings() {
        String query = getArguments().getString(ARG_QUERY);
        apiService.getSearch("MainImage", query, PAGE_SIZE, this);
    }

    @Override
    public void success(ListingResponse listingResponse, Response response) {
        getActivity().setProgressBarIndeterminateVisibility(false);
        adapter.addAll(listingResponse.getListings());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void failure(RetrofitError error) {
        getActivity().setProgressBarIndeterminateVisibility(false);
        Toast.makeText(getActivity(), R.string.unable_to_load_listings, Toast.LENGTH_SHORT).show();
    }

    private class ListingsAdapter extends ArrayAdapter<Listing> {

        private Random random;
        private SparseArray<Double> heightRatios = new SparseArray<Double>();

        public ListingsAdapter(Context context, List<Listing> objects) {
            super(context, -1, objects);
            random = new Random();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if(convertView == null) {
                convertView = LayoutInflater.from(getContext())
                        .inflate(R.layout.listing_item, parent, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }

            final Listing listing = getItem(position);
            Picasso.with(getContext()).load(listing.getImage().getUrl570xN()).into(holder.image);
            holder.image.setHeightRatio(getPositionRatio(position));
            holder.title.setText(listing.getTitle());
            holder.price.setText(String.format("%s %s", listing.getPrice(), listing.getCurrencyCode()));

            return convertView;
        }

        private double getPositionRatio(final int position) {
            double ratio = heightRatios.get(position, 0.0);
            if (ratio == 0) {
                ratio = getRandomHeightRatio();
                heightRatios.append(position, ratio);
            }
            return ratio;
        }

        private double getRandomHeightRatio() {
            return (random.nextDouble() / 2.0) + 1.0;
        }
    }

    class ViewHolder {
        @InjectView(R.id.listing_item_image) DynamicHeightImageView image;
        @InjectView(R.id.listing_item_title) TextView title;
        @InjectView(R.id.listing_item_price) TextView price;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

    private void launchWebViewActivity(Listing listing) {
        Intent intent = new Intent(getActivity(), WebViewActivity.class);
        intent.putExtra(WebViewActivity.EXTRA_TITLE, listing.getTitle());
        intent.putExtra(WebViewActivity.EXTRA_URL, listing.getUrl());
        startActivity(intent);
    }
}
