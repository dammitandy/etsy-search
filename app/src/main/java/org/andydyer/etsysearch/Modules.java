package org.andydyer.etsysearch;

import org.andydyer.etsysearch.api.ApiServiceModule;

/**
 *
 */
public class Modules {
    static Object[] list() {
        return new Object[] {
            new ApiServiceModule()
        };
    }
}
