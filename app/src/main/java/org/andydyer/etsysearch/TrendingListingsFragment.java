package org.andydyer.etsysearch;

/**
 *
 */
public class TrendingListingsFragment extends ListingsFragment {

    @Override
    protected void requestListings() {
        apiService.getTrending("MainImage", PAGE_SIZE, this);
    }
}
