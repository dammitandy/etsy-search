package org.andydyer.etsysearch.api;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;

/**
 *
 */
public class Listing {

    @Getter long listingId;
    @Getter long userId;
    @Getter long categoryId;
    @Getter String title;
    @Getter String description;
    @Getter String price;
    @Getter String currencyCode;
    @Getter String url;
    @Getter @SerializedName("MainImage") Image image;
}
