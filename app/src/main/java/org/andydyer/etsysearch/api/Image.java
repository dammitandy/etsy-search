package org.andydyer.etsysearch.api;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;

/**
 *
 */
public class Image {

    @Getter @SerializedName("url_75x75") String url75x75;
    @Getter @SerializedName("url_170x135")String url170x135;
    @Getter @SerializedName("url_570xN") String url570xN;
    @Getter @SerializedName("url_fullxfull")String urlFull;
}
