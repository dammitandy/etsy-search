package org.andydyer.etsysearch.api;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 *
 */
public interface ApiService {

    public static final String API_URL = "https://api.etsy.com/v2";

    @GET("/listings/trending")
    public void getTrending(@Query("includes") String includes, @Query("limit") int limit, Callback<ListingResponse> callback);

    @GET("/listings/active")
    public void getSearch(@Query("includes") String includes, @Query("keywords") String keywords, @Query("limit") int limit, Callback<ListingResponse> callback);
}
