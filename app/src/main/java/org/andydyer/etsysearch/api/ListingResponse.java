package org.andydyer.etsysearch.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;

/**
 *
 */
public class ListingResponse {

    @Getter int count;
    @Getter Pagination pagination;
    @Getter @SerializedName("results") List<Listing> listings;
}
