package org.andydyer.etsysearch.api;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.andydyer.etsysearch.EtsySearchApplication;
import org.andydyer.etsysearch.ListingsFragment;
import org.andydyer.etsysearch.R;
import org.andydyer.etsysearch.TrendingListingsFragment;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 *
 */
@Module(injects = {ListingsFragment.class, TrendingListingsFragment.class})
public class ApiServiceModule {

    @Provides @Singleton
    public ApiService provideApiService() {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        return new RestAdapter.Builder()
                .setEndpoint(ApiService.API_URL)
                .setConverter(new GsonConverter(gson))
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestInterceptor.RequestFacade request) {
                        request.addQueryParam("api_key",
                                EtsySearchApplication.getInstance().getString(R.string.api_key));
                    }
                })
                .build()
                .create(ApiService.class);
    }
}
