package org.andydyer.etsysearch.api;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;

/**
 *
 */
public class Pagination {

    @Getter @SerializedName("effective_page") int page;
    @Getter int nextPage;
}
