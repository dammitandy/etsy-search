package org.andydyer.etsysearch;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;

/**
 *
 */
public class SearchActivity extends Activity {

    public static final String EXTRA_QUERY = "query";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        setProgressBarIndeterminate(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        String query = getIntent().getStringExtra(EXTRA_QUERY);
        getActionBar().setTitle(getString(R.string.search_title, query));

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.container, ListingsFragment.newInstance(query))
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
