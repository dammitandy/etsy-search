package org.andydyer.etsysearch;

import android.app.Application;

import lombok.Getter;
import lombok.NonNull;

/**
 *
 */
public class EtsySearchApplication extends Application {

    private dagger.ObjectGraph objectGraph;

    @Getter static EtsySearchApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        objectGraph = dagger.ObjectGraph.create(Modules.list());
    }

    public void inject(@NonNull Object dependent) {
        objectGraph.inject(dependent);
    }
}
