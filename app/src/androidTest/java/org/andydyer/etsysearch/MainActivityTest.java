package org.andydyer.etsysearch;

import android.test.ActivityInstrumentationTestCase2;

import org.andydyer.etsysearch.api.Listing;

import static com.google.android.apps.common.testing.ui.espresso.Espresso.onData;
import static com.google.android.apps.common.testing.ui.espresso.Espresso.onView;
import static com.google.android.apps.common.testing.ui.espresso.action.ViewActions.click;
import static com.google.android.apps.common.testing.ui.espresso.assertion.ViewAssertions.matches;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.withId;
import static org.andydyer.etsysearch.test.CustomMatchers.hasData;
import static org.andydyer.etsysearch.test.CustomMatchers.showsUrl;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

/**
 *
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        getActivity();
    }

    public void testHasData() throws Exception {
        onView(withId(R.id.grid_view)).check(matches(hasData()));
    }

    public void testRowClickLaunchesListingUrl() throws Exception {
        onData(is(instanceOf(Listing.class))).atPosition(0).perform(click());
        onView(withId(R.id.webview)).check(matches(showsUrl("https://www.etsy.com/listing/196999735/duvet-cover-deep-ocean-blue-coastal?utm_source=iosteaminterviewapp&utm_medium=api&utm_campaign=api")));
    }
}
