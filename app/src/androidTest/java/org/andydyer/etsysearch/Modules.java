package org.andydyer.etsysearch;

import org.andydyer.etsysearch.api.MockApiServiceModule;

/**
 *
 */
public class Modules {
    static Object[] list() {
        return new Object[] {
            new MockApiServiceModule()
        };
    }
}
