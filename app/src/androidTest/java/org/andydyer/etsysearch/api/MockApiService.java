package org.andydyer.etsysearch.api;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Query;
import retrofit.mime.TypedByteArray;

/**
 *
 */
public class MockApiService implements ApiService {

    private static final int HTTP_OK_STATUS = 200;

    private static final String TRENDING_RESPONSE_FILE = "trending.json";
    private static final String ACTIVE_RESPONSE_FILE = "active.json";

    @Override
    public void getTrending(String includes, int limit, Callback<ListingResponse> callback) {
        getListingResponse(TRENDING_RESPONSE_FILE, callback);
    }

    @Override
    public void getSearch(String includes, String keywords, int limit, Callback<ListingResponse> callback) {
        getListingResponse(ACTIVE_RESPONSE_FILE, callback);
    }

    private void getListingResponse(String assetFile, Callback<ListingResponse> callback) {
        try {
            String json = readFileFromAssets(assetFile);
            Response response = getMockResponse(HTTP_OK_STATUS, json);
            ListingResponse listingResponse = getMockResponseData(json, ListingResponse.class);
            callback.success(listingResponse, response);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String readFileFromAssets(String fileName) throws IOException {
        InputStream stream = getClass().getClassLoader().getResourceAsStream("assets/" + fileName);
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
        return IOUtils.toString(reader);
    }

    private <T> T getMockResponseData(String json, Class<T> klass) {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        return gson.fromJson(json, klass);
    }

    private Response getMockResponse(int httpCode, String json) throws IOException {
        return new Response("url", httpCode, "", Collections.EMPTY_LIST,
                new TypedByteArray("application/json", json.getBytes()));
    }
}
