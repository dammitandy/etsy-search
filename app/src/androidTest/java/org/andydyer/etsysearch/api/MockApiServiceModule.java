package org.andydyer.etsysearch.api;

import org.andydyer.etsysearch.ListingsFragment;
import org.andydyer.etsysearch.TrendingListingsFragment;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 *
 */
@Module(injects = {ApiServiceTest.class, ListingsFragment.class, TrendingListingsFragment.class})
public class MockApiServiceModule {

    @Provides
    @Singleton
    public ApiService provideApiService() {
        return new MockApiService();
    }
}
