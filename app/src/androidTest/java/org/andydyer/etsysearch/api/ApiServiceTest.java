package org.andydyer.etsysearch.api;

import android.test.InstrumentationTestCase;

import org.andydyer.etsysearch.EtsySearchApplication;

import javax.inject.Inject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 *
 */
public class ApiServiceTest extends InstrumentationTestCase {

    @Inject ApiService apiService;

    @Override
    protected void setUp() throws Exception {
        EtsySearchApplication.getInstance().inject(this);
    }

    public void testSearchRequest() {
        apiService.getSearch("MainImage", "android", 25, listingCallback);
    }

    public void testTrendingRequest() {
        apiService.getTrending("MainImage", 25, listingCallback);
    }

    private Callback<ListingResponse> listingCallback = new Callback<ListingResponse>() {
        @Override
        public void success(ListingResponse listingResponse, Response response) {
            assertNotNull(listingResponse.getListings());
            assertFalse(listingResponse.getListings().isEmpty());
        }

        @Override
        public void failure(RetrofitError error) {

        }
    };
}
